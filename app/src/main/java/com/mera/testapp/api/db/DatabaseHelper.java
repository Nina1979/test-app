/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mera.testapp.api.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import com.mera.testapp.api.models.State;
import static com.mera.testapp.api.db.StateTable.TABLE_STATE;

import java.util.ArrayList;



public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, "database.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(StateTable.CREATE_TABLE_STATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public boolean insert(ArrayList<State> states) {
        if (states == null || states.isEmpty()) {
            return false;
        }

        boolean isAllInserted = true;

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues(4);
        for (State state : states) {
            cv.put(StateTable.KEY_STATE_ICAO, state.getIcao24());
            cv.put(StateTable.KEY_STATE_CALLSIGN, state.getCallsign());
            cv.put(StateTable.KEY_STATE_COUNTRY, state.getOriginCountry());
            cv.put(StateTable.KEY_STATE_VELOCITY, state.getVelocity());

            long rowId = db.insert(TABLE_STATE, null, cv);
            if (rowId == -1) {
                isAllInserted = false;
                break;
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return isAllInserted;
    }
    public ArrayList<State> query(String countryFilter, SortType type) {
        ArrayList<State> result = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor;
        if(TextUtils.isEmpty(countryFilter)) {
            cursor = db.query(TABLE_STATE, null, null, null, null, null, getSortString(type));
        } else {
            cursor = db.query(TABLE_STATE, null, StateTable.KEY_STATE_COUNTRY + " = ?", new String[]{countryFilter}, null, null, getSortString(type));
        }
        while (cursor.moveToNext()) {
            result.add(StateTable.convert(cursor));
        }

        return result;
    }

    public ArrayList<String> queryCountries() {
        ArrayList<String> result = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.query(true,TABLE_STATE, new String[]{StateTable.KEY_STATE_COUNTRY}, null,  null, null, null,StateTable.KEY_STATE_COUNTRY + " ASC",null);

        while (cursor.moveToNext()) {
            result.add(cursor.getString(0));
        }
        cursor.close();
        return result;
    }

    public void delete() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_STATE, null, null);
    }

    private String getSortString(SortType sortType) {
        if(sortType == SortType.NONE) {
            return "";
        }

        String field;

        switch(sortType) {
            case VEL_ASC:
                field = StateTable.KEY_STATE_VELOCITY;
                break;
            case VEL_DESC:
                field = StateTable.KEY_STATE_VELOCITY;
                break;
            case SIGN_ASC:
                field = StateTable.KEY_STATE_CALLSIGN;
            case SIGN_DESC:
                field = StateTable.KEY_STATE_CALLSIGN;
                break;
            default:
                field = StateTable.KEY_STATE_COUNTRY;
                break;
        }

        String orderString = sortType.name().contains("ASC") ? "ASC" : "DESC";

        return field + " " + orderString;
    }

    public enum SortType {
        NONE, VEL_ASC, VEL_DESC, SIGN_ASC, SIGN_DESC,
    }
}
