/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mera.testapp.api.rest;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.mera.testapp.api.db.DatabaseHelper;
import com.mera.testapp.api.models.State;
import com.mera.testapp.api.models.States;

import retrofit2.Call;

import java.io.IOException;
import java.util.ArrayList;

public class WebService extends Service {
    public static final String STATES_UPDATED_ACTION = "states_updated";
    private final LocalBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder
    {
        public WebService getService()
        {
            return WebService.this;
        }
    }

    public static Intent createServiceIntent(Context context)
    {
        return new Intent(context, WebService.class);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    public ArrayList<State> getStatesLocal(Context context, String countryFilter) {
        return new DatabaseHelper(context).query(countryFilter, DatabaseHelper.SortType.NONE);
    }

    public ArrayList<String> getLocalCountries(Context context) {
        return new DatabaseHelper(context).queryCountries();
    }

    public void requestStates(final Context context)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                ArrayList<State> statesArray = new ArrayList<>();
                WebApiManager wm = new WebApiManager();
                Call<States> call = wm.getWebApiInterface().getStates();
                try
                {
                   States states = wm.execute(call);
                    if (states != null)
                    {
                        statesArray = states.getStates();
                    }
                } catch (IOException | RuntimeException e) {
                    e.printStackTrace();
                }

                DatabaseHelper helper = new DatabaseHelper(context);
                helper.delete();
                helper.insert(statesArray);

                sendBroadcast(new Intent(STATES_UPDATED_ACTION));
            }
        }).start();
    }
}
