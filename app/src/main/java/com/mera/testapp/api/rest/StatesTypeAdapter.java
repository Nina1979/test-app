/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mera.testapp.api.rest;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.mera.testapp.api.models.State;

import java.io.IOException;

class StatesTypeAdapter extends TypeAdapter<State> {

    @Override
    public void write(JsonWriter out, State value) throws IOException {

    }

    public State read(JsonReader reader) throws IOException {
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull();
            return null;
        }
        reader.beginArray();
        String mIcao24 = reader.nextString();
        String mCallsign = reader.nextString();
        String mOriginCountry = reader.nextString();


        if (reader.peek() != JsonToken.NULL)  reader.nextDouble();
        else reader.nextNull();


        if (reader.peek() != JsonToken.NULL) reader.nextDouble();
        else reader.nextNull();


        if (reader.peek() != JsonToken.NULL) reader.nextDouble();
        else reader.nextNull();


        if (reader.peek() != JsonToken.NULL)  reader.nextDouble();
        else reader.nextNull();


        if (reader.peek() != JsonToken.NULL)  reader.nextDouble();
        else reader.nextNull();


        reader.nextBoolean();

        double mVelocity = 0;
        if (reader.peek() != JsonToken.NULL)
            mVelocity = reader.nextDouble();
        else reader.nextNull();


        if (reader.peek() != JsonToken.NULL) reader.nextDouble();
        else reader.nextNull();


        if (reader.peek() != JsonToken.NULL) reader.nextDouble();
        else reader.nextNull();

        reader.nextNull();

        if (reader.peek() != JsonToken.NULL) reader.nextDouble();
        else reader.nextNull();

        if (reader.peek() != JsonToken.NULL)
            reader.nextString();
        else
            reader.nextNull();
        reader.nextBoolean();
        reader.nextInt();
        reader.endArray();
        return new State(mIcao24, mCallsign, mOriginCountry, (float)mVelocity);
    }
}