/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mera.testapp.ui;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mera.testapp.R;
import com.mera.testapp.activities.MainActivity;
import com.mera.testapp.api.rest.WebService;
import com.mera.testapp.api.models.State;

import java.util.ArrayList;

public class ListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = ListFragment.class.getSimpleName();


    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ListAdapter mAdapter;

    private WebService mService;
    private boolean mIsServiceBound;

    private StatesReceiver mStatesReceiver;

    private String mCountryFilter;
    private int mChosenFilterPosition;

    public ListFragment(){}


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.list_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new ListAdapter();
        recyclerView.setAdapter(mAdapter);

        registerReceivers();

        getContext().bindService(WebService.createServiceIntent(getContext()), mConnection, Context.BIND_AUTO_CREATE);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if(itemId == R.id.list_filter) {
            showFilterDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unregisterReceivers();
        try {
            getContext().unbindService(mConnection);

            mIsServiceBound = false;
            mService = null;
        } catch (RuntimeException e) {
            Log.e(TAG, "An error occurred during the mService stop.", e);
        }
    }

    private void registerReceivers() {
        if (mStatesReceiver == null) {
            mStatesReceiver = new StatesReceiver();
            getContext().registerReceiver(mStatesReceiver, new IntentFilter(WebService.STATES_UPDATED_ACTION));
        }
    }

    private void unregisterReceivers() {
        if (mStatesReceiver != null) {
            try {
                getContext().unregisterReceiver(mStatesReceiver);
            } catch (RuntimeException e) {
                Log.e(TAG, "Can't unregister StatesReceiver", e);
            }
        }
        mStatesReceiver = null;
    }


    private class StatesReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "StatesReceiver: onReceive action: " + intent.getAction());
            try {
                ArrayList<State> localStates = mService.getStatesLocal(getContext(), mCountryFilter);
                if (localStates != null && !localStates.isEmpty()) {
                    mAdapter.setData(localStates);
                    MainActivity activity = (MainActivity) getActivity();
                    activity.updateActionBar(Integer.toString(localStates.size()));
                } } catch (RuntimeException e) {
                e.printStackTrace();
            }


            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        if (isServiceAvailable()) {
            mSwipeRefreshLayout.setRefreshing(true);
            mService.requestStates(getContext());
        }
    }

    private void showFilterDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        ArrayList<String> t = mService.getLocalCountries(getContext());
        t.add(0,"All");
        final String[] choices =  t.toArray(new String[t.size()]);

        builder.setSingleChoiceItems(choices, mChosenFilterPosition, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mChosenFilterPosition = which;
                if(which == 0) {
                    mCountryFilter = "";
                } else {
                    mCountryFilter = choices[which];
                }
                getContext().sendBroadcast(new Intent(WebService.STATES_UPDATED_ACTION));
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private boolean isServiceAvailable() {
        return mService != null && mIsServiceBound;
    }


    private final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mIsServiceBound = true;

            WebService.LocalBinder mBinder = (WebService.LocalBinder) iBinder;
            mService = (mBinder).getService();
            ArrayList<State> localStates = mService.getStatesLocal(getContext(), mCountryFilter);
            if (localStates != null && !localStates.isEmpty()) {
                mAdapter.setData(localStates);
                MainActivity activity = (MainActivity) getActivity();
                activity.updateActionBar(Integer.toString(localStates.size()));
            }

            if (isServiceAvailable()) {
                mSwipeRefreshLayout.setRefreshing(true);
                mService.requestStates(getContext());
            }
         }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mIsServiceBound = false;
            mService = null;
        }
    };
}
