/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mera.testapp.api.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Objects;

public class State implements Parcelable {
    private final String mIcao24;
    private final String mCallsign;
    private final String mOriginCountry;
    private float mTimePosition;
    private float mTimeVelocity;
    private float mLongitude;
    private float mLatitude;
    private float mAltitude;
    private boolean mIsOnGround;
    private final float mVelocity;
    private float mHeading;
    private float mVerticalRate;

    public State(String icao, String callsign, String country, float velocity) {
        this.mIcao24 = icao;
        this.mCallsign = callsign;
        this.mOriginCountry = country;
        this.mVelocity = velocity;
    }

    private State(Parcel parcel) {
        mIcao24 = parcel.readString();
        mCallsign = parcel.readString();
        mOriginCountry = parcel.readString();
        mTimePosition = parcel.readFloat();
        mTimeVelocity = parcel.readFloat();
        mLongitude = parcel.readFloat();
        mLatitude = parcel.readFloat();
        mAltitude = parcel.readFloat();
        mIsOnGround = parcel.readInt() == 1;
        mVelocity = parcel.readFloat();
        mHeading = parcel.readFloat();
        mVerticalRate = parcel.readFloat();
    }

    public String getIcao24() {
        return mIcao24;
    }

    public String getCallsign() {
        return mCallsign;
    }

    public String getOriginCountry() {
        return mOriginCountry;
    }

    public float getVelocity() {
        return mVelocity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mIcao24);
        dest.writeString(mCallsign);
        dest.writeString(mOriginCountry);
        dest.writeFloat(mTimePosition);
        dest.writeFloat(mTimeVelocity);
        dest.writeFloat(mLongitude);
        dest.writeFloat(mLatitude);
        dest.writeFloat(mAltitude);
        dest.writeInt(mIsOnGround ? 1 : 0);
        dest.writeFloat(mVelocity);
        dest.writeFloat(mHeading);
        dest.writeFloat(mVerticalRate);
    }

    public static final Parcelable.Creator<State> CREATOR = new Parcelable.Creator<State>() {
        public State createFromParcel(Parcel in) {
            return new State(in);
        }
        public State[] newArray(int size) {
            return new State[size];
        }
    };

    public static State parse(ArrayList<String> stateRaw) {
        return new State(stateRaw.get(0), stateRaw.get(1),
                stateRaw.get(2), Float.parseFloat(stateRaw.get(10)));
    }

    @Override
    public boolean equals(Object obj) {
        if (!obj.getClass().equals(State.class)){
            return false;
        }
        State state = (State) obj;
        return TextUtils.equals(mIcao24, state.mIcao24)
                && TextUtils.equals(mCallsign, state.mCallsign)
                && Objects.equals(mOriginCountry, state.mOriginCountry)
                && mVelocity == state.mVelocity;
    }

    @Override
    public int hashCode() {
        return (int) mVelocity;
    }
}
