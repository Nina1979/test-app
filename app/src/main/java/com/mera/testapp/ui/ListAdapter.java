/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mera.testapp.ui;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mera.testapp.R;
import com.mera.testapp.api.models.State;

import java.util.ArrayList;

@SuppressWarnings("unchecked")
class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private ArrayList<State> mDataset;

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mIcao24;
        TextView mCallsign;
        TextView mOriginCountry;
        TextView mVelocity;
        ViewHolder(View view) {
            super(view);
        }
    }


    ListAdapter() {
        mDataset = new ArrayList<>();
    }

    public void setData(ArrayList<State> dataset) {
        mDataset = dataset;
        notifyDataSetChanged();
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);

        vh.mIcao24 = (TextView) v.findViewById(R.id.icao24);
        vh.mCallsign = (TextView) v.findViewById(R.id.callsign);
        vh.mOriginCountry = (TextView) v.findViewById(R.id.origin_country);
        vh.mVelocity = (TextView) v.findViewById(R.id.velocity);

        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mIcao24.setText(mDataset.get(position).getIcao24());
        holder.mCallsign.setText(mDataset.get(position).getCallsign());
        holder.mOriginCountry.setText(mDataset.get(position).getOriginCountry());
        holder.mVelocity.setText(Float.toString(mDataset.get(position).getVelocity()));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
