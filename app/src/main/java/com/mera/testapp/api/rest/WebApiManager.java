/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mera.testapp.api.rest;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.testapp.api.models.State;

import okhttp3.OkHttpClient;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

class WebApiManager {
    private static final String API_ENDPOINT = "https://opensky-network.org/api/";
    private static Retrofit mRetrofit = null;
    private final WebApiInterface mWebApiInterface;

    WebApiManager() {
        mWebApiInterface = getRetrofit().create(WebApiInterface.class);
    }

    @NonNull
    WebApiInterface getWebApiInterface() {
        return mWebApiInterface;
    }

    @NonNull
    private static Retrofit getRetrofit() {

        if (mRetrofit == null) {
             Gson mGson = new GsonBuilder()
                    .registerTypeAdapter(State.class,new StatesTypeAdapter())
                    .create();
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(API_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create(mGson))
                    .client(getClient())
                    .build();

        }
        return mRetrofit;
    }

    private static OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS).build();
    }

    public  <T> T execute(Call<T> request) throws IOException {
        final Response<T> response = request.execute();
        return response.body();
    }
}
